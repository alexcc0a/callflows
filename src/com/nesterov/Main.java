package com.nesterov;

import java.util.Arrays;
import java.util.concurrent.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("Создаю потоки...");
        // Создаем объекты для четырех потоков.
        Callable<Integer> myCallFlows1 = new MyCallFlows("Поток № 1");
        Callable<Integer> myCallFlows2 = new MyCallFlows("Поток № 2");
        Callable<Integer> myCallFlows3 = new MyCallFlows("Поток № 3");
        Callable<Integer> myCallFlows4 = new MyCallFlows("Поток № 4");

        // Создаем пул потоков с фиксированным количеством потоков, основанном на доступных процессорах.
        ExecutorService threadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        // Отправляем задачи на выполнение в пул потоков.
        Future<Integer> task1 = threadPool.submit(myCallFlows1);
        Future<Integer> task2 = threadPool.submit(myCallFlows2);
        Future<Integer> task3 = threadPool.submit(myCallFlows3);
        Future<Integer> task4 = threadPool.submit(myCallFlows4);

        try {
            // Ожидаем завершения выполнения каждой задачи и выводим результат.
            System.out.println(myCallFlows1 + " выполнил задачу " + task1.get() + " раз(а)");
            System.out.println(myCallFlows2 + " выполнил задачу " + task2.get() + " раз(а)");
            System.out.println(myCallFlows3 + " выполнил задачу " + task3.get() + " раз(а)");
            System.out.println(myCallFlows4 + " выполнил задачу " + task4.get() + " раз(а)");
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        try {
            // Вызываем метод invokeAny, который вернет результат первой успешно завершившейся задачи.
            Integer result = threadPool.invokeAny(Arrays.asList(myCallFlows1, myCallFlows2, myCallFlows3, myCallFlows4));
            System.out.println("Самый большой результат: " + result + " раз(а)");
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        System.out.println("Потоки завершены!");
        // Завершаем пул потоков.
        threadPool.shutdown();
    }
}

