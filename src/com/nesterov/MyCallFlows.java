package com.nesterov;

import java.util.concurrent.Callable;

public class MyCallFlows implements Callable<Integer> {
    private final String name;

    public MyCallFlows(String name) {
        this.name = name;
    }

    @Override
    public Integer call() throws Exception {
        int count = 0;
        // Устанавливаем имя текущего потока.
        Thread.currentThread().setName(name);
        while (count <= 3) {
            // Задержка на 2500 миллисекунд (2.5 секунды).
            Thread.sleep(2500);
            System.out.println("Это " + Thread.currentThread().getName() + ". Hello World!");
            count++;
        }
        return count;
    }

    @Override
    public String toString() {
        return name;
    }
}